import json
import logging
import time
import os

import pywebpush
import requests

import db
from utils import fair_use_api
from log import setup_logging

setup_logging()

vapid_private_key = os.getenv("VAPID_PRIVATE_KEY", "private_key.pem")

logger = logging.getLogger(__name__)


def main():
    logger.info("Starting main loop")
    while True:
        events = db.get_active_events()
        logger.info(f"Checking {len(events)} events")
        for event in events:
            event_id, class_id = event.decode().split(":")
            previous_race_count = db.get_latest_race_count(event_id, class_id)
            new_results, results = check_for_new_results(
                event_id, class_id, previous_race_count
            )
            if new_results:
                publish_new_results(
                    event_id,
                    class_id,
                    results,
                    db.get_event_metadata(event_id, class_id),
                )
                db.save_latest_race_count(event_id, class_id, results["LastRaceIndex"])
        db.activate_events()
        db.remove_finished_events()
        time.sleep(10)


def publish_new_results(event_id: str, class_id: str, results: dict, event_data: dict):
    subscribers = db.get_subscribers(event_id, class_id)
    logger.info(
        f"New results for {event_data['name']}:{results['RegattaName']}: {results['LastRaceIndex']} available. "
        f"Pushing to {len(subscribers)} subscribers"
    )
    for subscriber in subscribers:
        subscription = db.get_subscriber_contact(subscriber)
        data = {
            "title": f"{event_data['name']}: {results['RegattaName']} Results",
            "message": f"Results for race {results['LastRaceIndex']} are now available",
            "resultUrl": f"https://www.manage2sail.com/de-DE/event/{event_id}#!/results?classId={class_id}",  # TODO: maybe store in db
        }
        push_results(subscription, data)


def push_results(subscription: dict, data: dict):
    pywebpush.webpush(
        subscription_info=subscription,
        data=json.dumps(data),
        vapid_private_key=vapid_private_key,
        vapid_claims={"sub": "mailto:example@x.com"},
        ttl=60 * 60 * 6,  # 6 hours
    )


@fair_use_api(min_time=60, max_time=150)
def get_results(event_id: str, class_id: str):
    url = f"https://www.manage2sail.com/api/event/{event_id}/regattaresult/{class_id}"
    response = requests.get(url)
    return response.json()


def check_for_new_results(event_id: str, class_id: str, previous_race_count: int):
    results = get_results(event_id, class_id)
    if "LastRaceIndex" in results and results["LastRaceIndex"] > previous_race_count:
        return True, results
    return False, results


if __name__ == "__main__":
    main()
