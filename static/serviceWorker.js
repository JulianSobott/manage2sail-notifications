self.addEventListener('push', event => {
    console.log('Push received in service worker!')
    console.log(event.data.json());
    const data = event.data.json();
    const { title, message, resultUrl } = data;

    const options = {
        body: message,
        icon: 'icon.png',
        badge: 'icon.png',
        data: { url: resultUrl},
    };

    event.waitUntil(self.registration.showNotification(title, options));
});

self.addEventListener('notificationclick', function(event) {
    console.log('Notification clicked!');
    const notification = event.notification;
    const targetUrl = notification.data.url;
    console.log('URL:', targetUrl);

    notification.close();

    // Open the website URL in a new window/tab
    event.waitUntil(
        clients.openWindow(targetUrl)
    );
});
