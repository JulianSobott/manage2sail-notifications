const subscribeButton = document.getElementById('subscribe');

function getCookie(cname) {
    let name = cname + "=";
    let decodedCookie = decodeURIComponent(document.cookie);
    let ca = decodedCookie.split(';');
    for (let i = 0; i < ca.length; i++) {
        let c = ca[i];
        while (c.charAt(0) === ' ') {
            c = c.substring(1);
        }
        if (c.indexOf(name) === 0) {
            return c.substring(name.length, c.length);
        }
    }
    return undefined;
}

function getSubscriberIdOrUndefined() {
    return getCookie('subscriberID');
}

function saveSubscriberId(subscriberID) {
    document.cookie = `subscriberID=${subscriberID}; SameSite=Strict; Secure`;
}

const NO_SUBSCRIPTIONS_HTML = 'No subscriptions found.';

function showSubscriptions() {
    const subscriberID = getSubscriberIdOrUndefined();
    const subscriptionsDiv = document.getElementById('subscriptions');
    if (subscriberID) {
        fetch(`/subscriptions/${subscriberID}`).then(response => {
            if (!response.ok) {
                showNotification('Failed to fetch subscriptions from the server.', 'error');
                throw new Error('Failed to fetch subscriptions from the server.');
            } else {
                return response.json();
            }
        }).then(data => {
            if (data.length === 0) {
                subscriptionsDiv.innerHTML = NO_SUBSCRIPTIONS_HTML;
            } else {
                subscriptionsDiv.innerHTML = '';
                data.forEach(subscription => {
                    addSubscription(subscription, subscriptionsDiv)
                });
            }
        }).catch(error => {
            console.error(error);
        })
    } else {
        subscriptionsDiv.innerHTML = NO_SUBSCRIPTIONS_HTML;
    }
}

function addSubscription(subscription, subscriptionsDiv) {
    const {name, start, end, id} = subscription;
    // div with inner divs and button to unsubscribe
    const subscriptionDiv = document.createElement('div');
    subscriptionDiv.classList.add('subscription');
    const nameDiv = document.createElement('div');
    nameDiv.classList.add('name');
    nameDiv.innerText = name;
    const dateDiv = document.createElement('div');
    dateDiv.classList.add('date');
    dateDiv.innerText = `${start} - ${end}`;
    const unsubscribeButton = document.createElement('button');
    unsubscribeButton.innerText = 'Unsubscribe';
    const subscriberID = getSubscriberIdOrUndefined();
    unsubscribeButton.addEventListener('click', async () => {
        await fetch(`/subscriptions/${subscriberID}/${id}`, {
            method: 'DELETE'
        }).then(response => {
            if (!response.ok) {
                throw new Error('Failed to unsubscribe from the event.');
            } else {
                subscriptionDiv.remove();
            }
        }).catch(error => {
            console.error(error);
            showNotification('Failed to unsubscribe from the event.', 'error')
        })
    });
    subscriptionDiv.appendChild(nameDiv);
    subscriptionDiv.appendChild(dateDiv);
    subscriptionDiv.appendChild(unsubscribeButton);
    if (subscriptionsDiv.innerHTML === NO_SUBSCRIPTIONS_HTML) {
        subscriptionsDiv.innerHTML = '';
    }
    subscriptionsDiv.appendChild(subscriptionDiv);
}

function showNotification(text, level) {
    const notification = document.getElementById('notification');
    notification.innerText = text;
    notification.classList.add(level);
    setTimeout(() => {
        notification.innerText = '';
        notification.classList.remove(level);
    }, 5000);
}

document.addEventListener('DOMContentLoaded', showSubscriptions);

subscribeButton.addEventListener('click', async () => {
    // Check if service workers are supported
    if ('serviceWorker' in navigator && 'PushManager' in window) {
        try {
            // Register the service worker
            const registration = await navigator.serviceWorker.register('/static/serviceWorker.js');
            console.log('Service Worker registered!');

            // Request permission for notifications
            const permission = await Notification.requestPermission();
            if (permission !== 'granted') {
                alert('Permission for notifications was denied');
                return;
            }

            // Subscribe to push notifications
            const subscription = await registration.pushManager.subscribe({
                userVisibleOnly: true,
                applicationServerKey: urlBase64ToUint8Array('BNaZ2IEep_-RJrlyxlYVD5L4f47UxFWiUGhZBs7ncOFHNQbQd7NhPiTMskOxX_shAo7lnyXBJrg02NZF7MERCic')
            });

            const inputField = document.getElementById('tfEventUrl');
            const eventUrl = inputField.value;
            const mustContain = "manage2sail.com/";
            if (!eventUrl || !eventUrl.includes(mustContain)) {
                showNotification(`Event URL must be a valid Manage2Sail URL starting with ${mustContain}`, 'error');
                return;
            }

            const data = {
                // subscriberID: await getSubscriberID(),   // Ignore for now and add maybe later
                subscription,
                eventUrl
            };

            // Send subscription to the backend
            await fetch('/subscribe', {
                method: 'POST',
                headers: {
                    'Content-Type': 'application/json',
                },
                body: JSON.stringify(data)
            }).then(response => {
                return response.json();
            }).then(data => {
                if (data.error) {
                    showNotification(data.error, 'error');
                    return;
                }
                saveSubscriberId(data.subscriber_id);
                addSubscription(data.subscription, document.getElementById('subscriptions'));
                showNotification('Successfully subscribed to event!', 'success');
            }).catch(error => {
                console.error(error);
                showNotification(`Failed to subscribe to event with error: ${error}`, 'error');
            })

        } catch (error) {
            console.error('Service Worker registration failed:', error);
            showNotification(`Service Worker registration failed with error: ${error}`, 'error')
        }
    } else {
        console.error('Service Workers or Push Notifications are not supported in this browser.');
        showNotification('Service Workers or Push Notifications are not supported in this browser.', 'error')
    }
});

// Utility function to convert VAPID key
function urlBase64ToUint8Array(base64String) {
    const padding = '='.repeat((4 - base64String.length % 4) % 4);
    const base64 = (base64String + padding)
        .replace(/\-/g, '+')
        .replace(/_/g, '/');
    const rawData = window.atob(base64);
    const outputArray = new Uint8Array(rawData.length);
    for (let i = 0; i < rawData.length; ++i) {
        outputArray[i] = rawData.charCodeAt(i);
    }
    return outputArray;
}
