import json
import logging
from datetime import date
import hashlib
import os

import redis

redis_host = os.getenv("REDIS_HOST", "localhost")
db = redis.Redis(host=redis_host, port=6379, db=0)

logger = logging.getLogger(__name__)


def get_active_events():
    return db.smembers("active_events")


def event_exists(event_id: str, class_id: str):
    return db.sismember("events", f"{event_id}:{class_id}") or False


def get_event_metadata(event_id: str, class_id: str):
    start = db.get(f"{event_id}:{class_id}:start").decode()
    end = db.get(f"{event_id}:{class_id}:end").decode()
    start, end = date.fromisoformat(start), date.fromisoformat(end)
    name = db.get(f"{event_id}:{class_id}:name").decode()
    return {"start": start, "end": end, "name": name}


def get_subscribers(event_id: str, class_id: str) -> set[str]:
    return set(
        map(lambda x: x.decode(), db.smembers(f"{event_id}:{class_id}:subscribers"))
    )


def subscribe_to_event(event_id: str, class_id: str, subscription: dict):
    if not db.sismember("events", f"{event_id}:{class_id}"):
        raise ValueError("Event does not exist")
    subscriber_id = hashlib.sha256(json.dumps(subscription).encode()).hexdigest()
    db.sadd(f"{event_id}:{class_id}:subscribers", subscriber_id)
    db.sadd(f"subscriptions:{subscriber_id}", f"{event_id}:{class_id}")
    db.set(f"subscriber-contact:{subscriber_id}", json.dumps(subscription))
    return subscriber_id


def unsubscribe_from_event(subscriber_id: str, event_id: str, class_id: str):
    db.srem(f"{event_id}:{class_id}:subscribers", subscriber_id)
    db.srem(f"subscriptions:{subscriber_id}", f"{event_id}:{class_id}")
    if db.scard(f"{event_id}:{class_id}:subscribers") == 0:
        remove_event(event_id, class_id)


def get_subscriptions_with_meta_data(subscriber_id: str) -> list[dict]:
    subscriptions = db.smembers(f"subscriptions:{subscriber_id}")
    events = []
    for subscription in subscriptions:
        event_id, class_id = subscription.decode().split(":")
        subscription_data = get_subscription_with_meta_data(
            subscriber_id, event_id, class_id
        )
        assert subscription_data, f"Subscription {event_id}:{class_id} not found"
        events.append(subscription_data)
    return events


def get_subscription_with_meta_data(
    subscriber_id: str, event_id: str, class_id: str
) -> dict:
    if db.sismember(f"subscriptions:{subscriber_id}", f"{event_id}:{class_id}"):
        event_data = get_event_metadata(event_id, class_id)
        return {
            "event_id": event_id,
            "class_id": class_id,
            **event_data,
            "id": f"{event_id}:{class_id}",
        }
    return {}


def save_latest_race_count(event_id: str, class_id: str, race_count: int):
    db.set(f"{event_id}:{class_id}:race_count", race_count)


def get_latest_race_count(event_id: str, class_id: str) -> int:
    return int(db.get(f"{event_id}:{class_id}:race_count") or 0)


def add_event_if_not_exists(
    event_id: str, class_id: str, start: date, end: date, name: str
):
    if not db.sismember("events", f"{event_id}:{class_id}"):
        add_new_event(event_id, class_id, start, end, name)


def add_new_event(event_id: str, class_id: str, start: date, end: date, name: str):
    db.set(f"{event_id}:{class_id}:start", start.isoformat())
    db.set(f"{event_id}:{class_id}:end", end.isoformat())
    db.set(f"{event_id}:{class_id}:name", name)
    db.sadd("events", f"{event_id}:{class_id}")
    if is_event_active(event_id, class_id):
        activate_event(event_id, class_id)


def activate_event(event_id: str, class_id: str):
    db.sadd("active_events", f"{event_id}:{class_id}")


def remove_event(event_id: str, class_id: str):
    db.delete(f"{event_id}:{class_id}:start")
    db.delete(f"{event_id}:{class_id}:end")
    db.srem("events", f"{event_id}:{class_id}")
    db.srem("active_events", f"{event_id}:{class_id}")
    db.delete(f"{event_id}:{class_id}:race_count")
    db.delete(f"{event_id}:{class_id}:subscribers")
    db.delete(f"{event_id}:{class_id}:name")


def is_event_active(event_id: str, class_id: str):
    now = date.today()
    start = date.fromisoformat(db.get(f"{event_id}:{class_id}:start").decode())
    end = date.fromisoformat(db.get(f"{event_id}:{class_id}:end").decode())
    return start <= now <= end


def is_event_finished(event_id: str, class_id: str):
    now = date.today()
    end = date.fromisoformat(db.get(f"{event_id}:{class_id}:end").decode())
    return now > end


def remove_finished_events():
    for event in db.smembers("events"):
        event_id, class_id = event.decode().split(":")
        if is_event_finished(event_id, class_id):
            remove_event(event_id, class_id)
            logger.info(f"Removed event {event_id}:{class_id}")


def activate_events():
    for event in db.smembers("events"):
        event_id, class_id = event.decode().split(":")
        if not db.sismember(
            "active_events", f"{event_id}:{class_id}"
        ) and is_event_active(event_id, class_id):
            activate_event(event_id, class_id)
            logger.info(f"Activated event {event_id}:{class_id}")


def get_subscriber_contact(subscriber_id: str):
    return json.loads(db.get(f"subscriber-contact:{subscriber_id}").decode())


def already_subscribed(event_id: str, class_id: str, subscription: dict):
    subscriber_id = hashlib.sha256(json.dumps(subscription).encode()).hexdigest()
    return db.sismember(f"{event_id}:{class_id}:subscribers", subscriber_id)
