from main import publish_new_results

if __name__ == "__main__":
    publish_new_results(
        "3b7d5024-dfd7-4c76-9f92-aa9c816a6300",
        "860b254f-1317-4622-a0f0-631630e615bc",
        {"RaceCount": 1, "RegattaName": "420"},
        {"name": "Demo Event"},
    )
