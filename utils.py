import random
import time
from datetime import date

import requests
from bs4 import BeautifulSoup


def fair_use_api(min_time: int, max_time: int):
    def decorator(func):
        def wrapper(*args, **kwargs):
            sleep_time = random.randint(min_time, max_time)
            time.sleep(sleep_time)
            return func(*args, **kwargs)

        return wrapper

    return decorator
