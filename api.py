import json
import re
from datetime import datetime

import requests
from bs4 import BeautifulSoup
from fastapi import FastAPI
from starlette.requests import Request
from starlette.responses import FileResponse, JSONResponse
from starlette.staticfiles import StaticFiles

import db
from utils import fair_use_api
from main import push_results

app = FastAPI()

static_files = "static"

app.mount("/static", StaticFiles(directory=static_files), name="static")

url_pattern = re.compile(
    r"https://(www.)?manage2sail.com/.*/event/(.*)#!/results\?classId=(.*)"
)

uuid_pattern = re.compile(
    r"[0-9a-f]{8}-[0-9a-f]{4}-[0-9a-f]{4}-[0-9a-f]{4}-[0-9a-f]{12}"
)


@app.get("/")
def read_root():
    return FileResponse("static/index.html")


@app.post("/subscribe")
async def subscribe(request: Request):
    body = await request.json()
    subscription: dict = body["subscription"]
    url = body["eventUrl"]
    match = url_pattern.match(url)
    if not match:
        return JSONResponse({"error": "Invalid URL"}, status_code=400)

    event_id, class_id = match.groups()[1:]
    if db.already_subscribed(event_id, class_id, subscription):
        return JSONResponse({"error": "Already subscribed"}, status_code=400)

    event_data = None
    # some urls have names instead of uuids, so we need to get the uuids first
    if not uuid_pattern.match(class_id) or not uuid_pattern.match(event_id):
        event_data = get_event_metadata(event_id, class_id)
        event_id, class_id = event_data["event_id"], event_data["class_id"]

    if not db.event_exists(event_id, class_id):
        if not event_data:
            event_data = get_event_metadata(event_id, class_id)
        db.add_event_if_not_exists(
            event_id,
            class_id,
            event_data["start"],
            event_data["end"],
            event_data["name"],
        )
    subscriber_id = db.subscribe_to_event(event_id, class_id, subscription)
    subscription_data = db.get_subscription_with_meta_data(
        subscriber_id, event_id, class_id
    )
    push_results(
        subscription,
        {
            "title": f"Subscribed to {subscription_data['name']}",
            "message": f"Notifications will start at {subscription_data['start'].isoformat()}",
            "resultUrl": url,
        },
    )
    return {"subscriber_id": subscriber_id, "subscription": subscription_data}


@app.delete("/subscriptions/{subscriber_id}/{subscription_id}")
async def unsubscribe(subscriber_id: str, subscription_id: str):
    event_id, class_id = subscription_id.split(":")
    db.unsubscribe_from_event(subscriber_id, event_id, class_id)
    return {"success": True}


@app.get("/subscriptions/{subscriber_id}")
async def get_subscriptions(subscriber_id: str):
    subscriptions = db.get_subscriptions_with_meta_data(subscriber_id)
    return subscriptions


@fair_use_api(min_time=1, max_time=2)
def get_event_metadata(event_id: str, class_id: str):
    url = f"https://www.manage2sail.com/en-US/event/{event_id}#!/results?classId={class_id}"
    html = requests.get(url).text
    soup = BeautifulSoup(html, "html.parser")
    dates_text = soup.find("span", {"class": "eventDates"}).text
    if "-" in dates_text:
        start_text, end_text = dates_text.split(" - ")
    else:
        start_text = end_text = dates_text
    # 09/07/2024
    start = datetime.strptime(start_text, "%d/%m/%Y").date()
    end = datetime.strptime(end_text, "%d/%m/%Y").date()

    event_name = soup.find("div", {"class": "eventName"}).find("h1").text

    if not uuid_pattern.match(class_id):
        # data is in <script> tag with window.boostrapedResourceData as json
        script = soup.find("script", text=re.compile("window.boostrapedResourceData"))
        data = script.string.split("window.boostrapedResourceData = ")[1].split(";")[0]
        json_data = json.loads(data)
        class_uuid = None
        for item in json_data["Regatta"]:
            if item["LinkIdOrAlias"] == class_id:
                class_uuid = item["Id"]
                break
        class_id = class_uuid

    if not uuid_pattern.match(event_id):
        # find <td> with uuid in it inside <script id="details" type="text/ng-template">
        script = soup.find("script", {"id": "details"})
        data = script.string
        s = BeautifulSoup(data, "html.parser")
        td = s.find("td", text=uuid_pattern)
        event_id = td.text

    return {
        "name": event_name,
        "start": start,
        "end": end,
        "class_id": class_id,
        "event_id": event_id,
    }
